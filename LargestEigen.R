##############################################################
### Code accompanying
### "Metapopulation persistence in random fragmented landscapes"
### J. Grilli, G. Barabás and S. Allesina
##############################################################
### Required R libraries
require(Matrix)
require(igraph)
##############################################################

##############################################################
### Largest eigenvalue and corresponding eigenvector for symmetric real matrices using arpack
### see http://www.inside-r.org/packages/cran/igraph/docs/arpack for documentation
### Function takes one parameter:
###     mymat: matrix  
###     valid parameters --> mymat: must be real and symmetric 
### The function returns a dataframe l1
### To access the largest eigenvalue: l1$values
### To access the largest eigenvector: l1$vectors

GetLargestEv <- function(mymat){
    func <- function(x, extra=NULL) { as.vector(mymat %*% x) }
    l1 <- arpack(func,
                  options=list(
                      n = dim(mymat)[1],
                      nev = 1, ncv = 10,
                      which = "LM",
                      maxiter = 200),sym=TRUE)
    return(l1)
}


